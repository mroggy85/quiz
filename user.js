﻿
var uuid = require('node-uuid');

var user = {
    _id: '',
    username: '',
    password: '',
    fbID: ''
};

exports.getUser = function (username, password, fbID) {
    if (username === undefined || 
        password === undefined || 
        fbID === undefined) {
        throw new Error('All params must be set');
    }
    
    var u = user;
    u._id = uuid.v4();
    u.username= username;
    u.password = password;
    u.fbID = fbID;
    
    return u;
};

/* Gets an object to be used when searching in te DB
 * */
exports.getUserCriteria = function (username, password) {
    return {
        username: username,
        password: password
    };
};

exports.getUserCriteriaOnlyUsername = function (username) {
    return {
        username: username
    };
};

exports.getUserCriteriaOnlyFacebookID = function (fbID) {
    return {
        fbID: fbID
    };
};