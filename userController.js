﻿
// external modules
var Q = require('q');

// Internal modules
var util = require('./libs/util.js');
var baseController = require('./baseController.js');

module.exports = function (api) {
    if (api === undefined) {
        throw new Error('api must be passed');
    }
    var _api = api;
    var _baseController = baseController(api);
    
    // Retrieve question
    var handleGet = function (req, res, isAdmin) {
        var uid = util.getQueryString(req, 'uid');
        if (uid === undefined) {
            return util.getErrorPromise(400, 'QueryString "uid" is not present');
        }
        
        return _api.getQuestion(uid, function (result) {
            return util.getOkPromise(result);
        });
    };
    
    // Answer a question
    var handlePost = function (req, res, isAdmin) {
        var uid = util.getQueryString(req, 'uid');
        var id = util.getQueryString(req, 'id');
        if (uid === undefined || 
            id === undefined) {
            return util.getErrorPromise(400, 'QueryString "uid" and "id" is not present');
        }
        
        return _api.answerQuestion(uid, id, function (result) {
            return util.getOkPromise(result);
        });
    };
    
    // (ADMIN) Create User
    var handlePut = function (req, res, isAdmin) {
        if (!isAdmin) {
            return util.getErrorPromise(401, 'Not admin', { 'WWW -Authenticate': 'Basic realm="quiz"' });
        }
        
        _baseController.extractJson(req)
        .then(function (json) { 
            
            var username = json.username;
            var password = json.password;
            var fbID = json.fbid;

            if (username === undefined ||
                password === undefined ||
                fbID === undefined) {
                return util.getErrorPromise(400, 'The data "username", "password" and "fbID" is not all present');
            }
            
            _api.getUserByUsername(username, function (users) {
                if (users.length > 0) {
                    return util.getErrorPromise(400, 'The username already exists');
                }
                
                _api.getUserByFacebookID(fbID, function (users) { 
                    if (users.length > 0) {
                        return util.getErrorPromise(400, 'The username already exists');
                    }

                    _api.createUser(username, password, fbID, function (result) { 
                        return util.getOkPromise(result);
                    });
                });
                
                
            });
        });
    };
    
    // (ADMIN) Delete question
    var handleDelete = function (req, res) {
        var uid = util.getQueryString(req, 'uid');
        if (uid === undefined) {
            return util.getErrorPromise(400, 'QueryString "uid" is not present');
        }
        
        _api.removeQuestion(uid, function (result) {
            return util.getOkPromise(result);
        });
    };
    
    _baseController.events.onGet = handleGet;
    _baseController.events.onPost = handlePost;
    _baseController.events.onPut = handlePut;
    _baseController.events.onDelete = handleDelete;
    
    return {
        'handleRequest' : _baseController.handleRequest
    };
};