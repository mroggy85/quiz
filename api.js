﻿
// external modules
var Q = require('q');

// --------------------------------
module.exports = function () {
    
    var dataPersiter = require('./libs/dataPersister.js');
    var _question = require('./question.js');
    var _user = require('./user.js');
    
    var DB = 'quizDb',
        COL_QUESTION = 'questions',
        COL_USERS = 'users';
    
    var _dbAdapter = dataPersiter({
        "dbName" : DB,
        "isDebug" : true
    });

    GetQuestionById = function (uid) {

        return _dbAdapter.get(COL_QUESTION, uid)
        .then(function (item) {
            console.dir(item);

            delete item['answer']; // remove correct answer
            return Q.resolve(item);
        });
    };
    
    AnswerQuestion = function (uid, id) {
        return _dbAdapter.get(COL_QUESTION, uid)
        .then(function (item) {
            console.log('correct answer: ' + item.answer);
            console.log('supplied answer: ' + id);
            var result = (item.answer == id);
            //callback(result);
            return Q.resolve(result);
        });
    };
    
    CreateQuestion = function (text, alternatives, answer) {
        var q = _question.getQuestion(text, alternatives, answer);

        return _dbAdapter.create(COL_QUESTION, q);
    };
    
    RemoveQuestion = function (uid, callback) { 
        
        _dbAdapter.remove(COL_QUESTION, uid, callback);
    };
    
    GetUserByUid = function (uid, callback) { 
        _dbAdapter.get(COL_USERS, uid, callback);
    };
    
    GetUserByUsername = function (username, callback) {
        username = username.trim();

        var criteria = _user.getUserCriteriaOnlyUsername(username);
        _dbAdapter.getAllByCriteria(COL_USERS, criteria, callback);
    };
    
    GetUserByFacebookID = function (fbID) {
        fbID = fbID.trim();
        
        var criteria = _user.getUserCriteriaOnlyFacebookID(fbID);
        _dbAdapter.getAllByCriteria(COL_USERS, criteria);
    };
    
    GetUserByCredentials = function (username, password) {
        username = username.trim();
        password = password.trim();
        
        var criteria = _user.getUserCriteria(username, password);
        return _dbAdapter.getAllByCriteria(COL_USERS, criteria);
    };
    
    CreateUser = function (username, password, fbID, callback) {
        var user = _user.getUser(username, password, fbID);

        _dbAdapter.create(COL_USERS, user, callback);
    };
    
    RemoveUser = function (uid, callback) {
        _dbAdapter.remove(COL_USERS, uid, callback);
    };

    var fascade = {
        'getQuestion': GetQuestionById,
        'answerQuestion': AnswerQuestion,
        'createQuestion': CreateQuestion,
        'removeQuestion': RemoveQuestion,

        'getUser': GetUserByUid,
        'getUserByCredentials': GetUserByCredentials,
        'getUserByUsername': GetUserByUsername,
        'getUserByFacebookID': GetUserByFacebookID,
        'createUser': CreateUser,
        'removeUser': RemoveUser
    };

    return fascade;
};