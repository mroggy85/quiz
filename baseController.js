﻿
// external modules
var url = require('url');
var Q = require('q');

// Internal modules
var util = require('./libs/util.js');

module.exports = function (api) {
    if (api === undefined) {
        throw new Error('api must be passed');
    }
    var _api = api;
    
    HandleRequest = function (obj) {
        var req = obj.req;
        var res = obj.res;
        var isAdmin = obj.isAdmin;
        
        if (req === undefined || 
            res === undefined || 
            isAdmin === undefined) {
            return util.getInternalErrorPromise('All params must be defined');
        }
        
        return routeToMethod(req, res, isAdmin);
    };
    
    var routeToMethod = function (req, res, isAdmin) {

        switch (req.method) {
            case 'GET':
                console.log('Base_Controller: GET');
                return fascade.events.onGet(req, res, isAdmin);

            case 'POST':
                console.log('Base_Controller: POST');
                return fascade.events.onPost(req, res, isAdmin);

            case 'PUT':
                console.log('Base_Controller: PUT');
                return fascade.events.onPut(req, res, isAdmin);

            case 'DELETE':
                console.log('Base_Controller: DELETE');
                if (!isAdmin) {
                    return util.getErrorPromise(401, 'Not admin', { 'WWW -Authenticate': 'Basic realm="quiz"' });
                }

                return fascade.events.onDelete(req, res, isAdmin);

            default:
                return util.getErrorPromise(400, 'HTTP method not known');
        }

        //return deferred.promise;
    };
    
    var ExtractJson = function (req) {
        var deferred = Q.defer();

        var jsonString = '';
        req.on('data', function (data) {
            console.log('ExtractJson-event: data: ' + data);
            jsonString += data;
        });
        req.on('end', function () {
            console.log('ExtractJson-event: end');
            var json = JSON.parse(jsonString);
            return deferred.resolve(json);
        });

        return deferred.promise;
    };
    
    var fascade = {
        'handleRequest' : HandleRequest,
        // All events must be implemented by the child class
        'events': {
            onGet: function () { throw new Error('Not implemented') },
            onPost: function () { throw new Error('Not implemented') },
            onPut: function () { throw new Error('Not implemented') },
            onDelete: function () { throw new Error('Not implemented') },
        },
        'extractJson': ExtractJson
    };
    
    return fascade;
    
};