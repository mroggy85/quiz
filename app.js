﻿console.log();
console.log('Quiz app starting...');

var http = require('http');
var Q = require('q');

var mainController = require('./mainController.js')();

http.createServer(function (req, res) {
    console.log('req: ' + req.url);

    if (req.url === '/favicon.ico') { 
        return;
    }
    
    try {
        mainController.handleRequest(req, res);
    }
    catch (err) {
        //console.error(err);
        console.log('-----------------------');
        console.log('--------ERROR----------');
        console.log('-----------------------');
        console.log('Stacktrace:')
        console.error(err.stack);

        res.writeHead(500);
        res.end();
    }

}).listen(1337);

console.log('Quiz app started!');
console.log('------------------------');