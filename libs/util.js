﻿var Q = require('q');
var url = require('url');

exports.getErrorPromise = function (code, reason, headers) {
    if (code === undefined ||
        reason === undefined) {
        console.error('code: ' + code);
        console.log('reason: ' + reason);
        return Q.reject(new Error('Code and Reason must be set!'));
    }
    if (headers === undefined) {
        return Q.reject({ code: code, reason: reason });
    }
    else {
        return Q.reject({ code: code, reason: reason, headers: headers });
    }
};

exports.getNiceErrorPromise = function (deferred, code, reason, headers) {
    if (code === undefined ||
            reason === undefined) {
        console.error('code: ' + code);
        console.log('reason: ' + reason);
        return deferred.reject(new Error('Code and Reason must be set!'));
    }
    if (headers === undefined) {
        return deferred.reject({ code: code, reason: reason });
    }
    else {
        return deferred.reject({ code: code, reason: reason, headers: headers });
    }
};

exports.getInternalErrorPromise = function (reason) {
    if (!reason) { 
        return Q.reject(new Error('reason must be set!'));
    }

    return Q.reject(new Error(reason));
}

exports.getOkPromise = function (result, optionals) { 
    return Q.resolve({ 'result': result, 'optionals': optionals});
};

exports.getQueryString = function (req, qs) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    return query[qs];
}