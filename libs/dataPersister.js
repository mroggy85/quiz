// external modules
var Q = require('q');

// Internal modules
var util = require('./util.js');

// Wrapper around MongoDB driver
// CRUD operations against the DB
// --------------------------------
module.exports = function (settings) {
    
    var defaultSettings = {
        "dbName" : "defaultDb",
        "dbHost" : "mongodb://localhost:27017/", // -- Local
        //"dbHost" : "mongodb://192.168.1.76:27017/", // -- LinuxServer
        "isDebug" : false
    };

    // Validate input
    if(!settings) {
        settings = defaultSettings;
    }
    else {
        if (!settings.dbName) { 
            settings.dbName = defaultSettings.dbName;
        }
        if (!settings.dbHost) {
            settings.dbHost = defaultSettings.dbHost;
        }
        if (!settings.isDebug) {
            settings.isDebug = defaultSettings.isDebug;
        }
    }
    

    // REQUIRES
    var events = require('events'),
    MongoClient = require('mongodb');

    // PRIVATE MEMBERS
    var _mongoClient = MongoClient.MongoClient;
    var _db;
    var _connecting = false;
    var isDebug = settings.isDebug;
    
    // CONSTANTS
    var DB_NAME = settings.dbName;
    var DB_HOST = settings.dbHost;

    // INIT
    var init = function () {
        if(isDebug) {
            console.log('DB-Adpater: Init');
            console.log('DB_NAME: ' + DB_NAME);
            console.log('DB_HOST: ' + DB_HOST);
        }

        connectToDb();
    };
    
    
    // PUBLIC METHODS

    var Create = function (col, obj) {
        if(isDebug) {
            console.log('DB_Adapter: Create');
            console.log('Collection: ' + col);
            console.log('obj:');
            console.dir(obj);
        }
        
        if(!col || !obj) {
            return Q.reject(new Error('All params must be present!'));
        }
        if (!obj._id) {
            return Q.reject(new Error('The property "_id" must be set'));
        }

        return save(col, obj);
    };

    var Get = function (col, uid) {
        if(isDebug) {
            console.log('DB_Adapter: Get');
            console.log('Collection: ' + col);
            console.log('uid: ' + uid);
        }
        
        if(!col || !uid) {
            return Q.reject(new Error('All params must be present!'));
        }

        return findOne(col, uid);
    };

    var GetAll = function (col, callback) {
        if(isDebug) {
            console.log('DB_Adapter: GetAll');
            console.log('Collection: ' + col);
        }
        
        if(!col || !callback) {
            throw new Error('All params must be present!');
        }

        findAll(col, callback);
    };
    
    var GetAllByCriteria = function (col, criteria) {
        if(isDebug) {
            console.log('DB_Adapter: GetAllByCriteria');
            console.log('Collection: ' + col);
            console.log('criteria:');
            console.dir(criteria);
        }

        if (!col || !criteria) {
            return Q.reject(new Error('All params must be present!'));
        }

        return findByCriteria(col, criteria);
    };

    var Update = function (col, obj, callback) {
        if(isDebug) {
            console.log('DB_Adapter: Update');
            console.log('Collection: ' + col);
            console.log('obj:');
            console.dir(obj);
        }
        
        if(!col || !obj || !callback) {
            throw new Error('All params must be present!');
        }

        update(col, obj, callback);
    };

    var Remove = function (col, uid, callback) {
        if(isDebug) {
            console.log('DB_Adapter: Remove');
            console.log('Collection: ' + col);
            console.log('uid: ' + uid);
        }
        
        if(!col || !uid || !callback) {
            throw new Error('All params must be present!');
        }

        removeOne(col, uid, callback);
    };
    
    
    // PRIVATE METHODS

    var save = function (collection, obj, callback) {
        if(isDebug) {
            console.log('DB_Adapter: [save] starting');
        }
        
        var deferred = Q.defer();

        var coll = getCollection(collection);
        coll.save(obj, {w: 1}, function (err, result) {
            if (err) {
                console.error(err.stack);
                return deferred.reject(err);
            }
            
            if(isDebug) {
                console.log('DB_Adapter: [save] succeded');
            }
            
            return deferred.resolve(result);
        });
        console.log('is syncronous?');

        return deferred.promise;
    };

    var insert = function (collection, obj, callback) {
        if(isDebug) {
            console.log('DB_Adapter: [insert] ...');
        }
        var coll = getCollection(collection);
        coll.insert(obj, {w: 1}, function (err, result) {
            if (err) {
                throw new Error(err);
            }
            
            if(isDebug) {
                console.log('DB_Adapter: [insert] succeded');
            }

            callback(obj);
        });
    };

    var update = function (collection, obj, callback) {
        if(isDebug) {
            console.log('DB_Adapter: [update] ...');
        }
        var coll = getCollection(collection);
        coll.update({ _id: obj._id }, obj, function (err, result) {
            if (err) {
                throw new Error(err);
            }
            
            if(isDebug) {
                console.log('DB_Adapter: [update] succeded');
            }

            callback(obj);
        });
    };

    var findAll = function (collection, callback) {
        if(isDebug) {
            console.log('DB_Adapter: [findAll] ...');
        }

        return findByCriteria(collection, {});
    };

    var findByCriteria = function (collection, criteria) {
        if(isDebug) {
            console.log('DB_Adapter: [findByCriteria] ...');
        }
        var coll = getCollection(collection);
        
        var deferred = Q.defer();
        
        coll.find(criteria).toArray(function (err, docs) { 
            if (err) {
                throw new Error(err);
            }

            console.log('docs:');
            console.log(docs.length);

            deferred.resolve(docs);
        });
        
        return deferred.promise;
    };

    var findOne = function (collection, uid) {
        if(isDebug) {
            console.log('DB_Adapter: [findOne] ...');
        }
        var coll = getCollection(collection);
        
        var deferred = Q.defer();

        coll.findOne({ _id: uid}, function (err, item) {
            if (err) {
                throw new Error(err);
            }
            
            if(isDebug) {
                console.log('DB_Adapter: [findOne] succeded');
                console.log('DB_Adapter: found: ' + item._id);
            }
            
            return deferred.resolve(item);
        });
        
        console.log('DB_Adapter: [findOne] returning promise');
        return deferred.promise;
    };

    var removeOne = function (collection, uid, callback) {
        if(isDebug) {
            console.log('DB_Adapter: [removeOne] ...');
        }
        if (!uid) {
            throw new Error("uid cannot be null");
        }

        var coll = getCollection(collection);
        coll.remove({ _id: uid}, {w: 1}, function (err, numberOfRemovedDocs) {
            if (err) {
                throw new Error(err);
            }
            
            if(isDebug) {
                console.log('DB_Adapter: [removeOne] succeded');
                console.log('DB_Adapter: docs removed: ' + numberOfRemovedDocs);
            }
            
            var result = (numberOfRemovedDocs === 1);
            callback(result);
        });
    };

    var docsToArray = function (docs, callback) {
        

        
        var deferred = Q.defer();
        
        docs.toArray(function (arr) {
            console.log('arr:');
            console.dir(arr);
            //return util.getOkPromise(docs);
            return deferred.resolve(arr);
        });

        return deferred.promise;
    };

    var connectToDb = function () {
        if(isDebug) {
            console.log('DB_Adapter: connecting to DB...');
        }
        
        MongoClient.connect(DB_HOST + DB_NAME, function (err, db) {
            if (err) {                
                throw 'DB ERROR: ' + err;
            }
            
            _db = db;
            
            if(isDebug) {
                console.log('DB_Adapter: connected to DB!');
            }
        });
    };

    var getCollection = function (collection) {
            return _db.collection(collection);
    };

    // FASCADE
    var fascade = {
        "create": Create,
        "get": Get,
        "update": Update,
        "remove": Remove,

        "getAll": GetAll,
        "getAllByCriteria": GetAllByCriteria
    };

    init();

    return fascade;
};
