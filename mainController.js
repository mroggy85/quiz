﻿
// external modules
var url = require('url');
var Q = require('q');

// internal modules
var api = require('./api.js');
var util = require('./libs/util.js');
var questionController = require('./questionController.js');
var userController = require('./userController.js');

module.exports = function () {
    
    var _api = api();
    var _userController = userController(_api);
    var _questionController = questionController(_api);
    
    HandleRequest = function (req, res) {
        
        authenticate(req, res)
        .then(function (obj) {
            console.log('routeToController');
            //console.dir(obj);
            return routeToController(obj);
        })
        .then(function (obj) {
            console.log('responding...');
            console.dir(obj);
            
            result = JSON.stringify(obj);
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(result);
        })
        .fail(function (err) {
            console.log('fail in main');
            console.dir(err);

            if (err.stack) {
                console.error(err.stack);
                res.writeHead(500, 'Internal Server Error');
                res.end();
            }
            else {
                if (err.headers === undefined) {
                    err.headers = {};
                }
                
                res.writeHead(err.code, err.reason, err.headers);
                res.end();
            }
        });
    };
    
    var routeToController = function (obj) {
        var req = obj.req;
        var res = obj.res;
        var isAdmin = obj.isAdmin;
        
        if (req === undefined || 
            res === undefined || 
            isAdmin === undefined) {
            throw new Error('All params must be defined');
        }
        
        var url_parts = url.parse(req.url, true);
        var pathname = url_parts.pathname;
        console.log(pathname);
        switch (pathname) {
            case '/question':
                console.log('Question');
                return _questionController.handleRequest(obj);

            case '/user':
                console.log('User');
                
                var uid = getQueryString(req, 'uid');
                if (!uid) {
                    res.writeHead(400);
                    res.end('Parameter "uid" not present');
                }
                
                _api.getUser(uid, function (result) {
                    respond(res, result);
                });
                break;
            default:
                return util.getErrorPromise(400, 'Controller not known, yo');
        }
    };
    
    var routeToMethod = function (obj) {
        var req = obj.req;
        var res = obj.res;
        var isAdmin = obj.isAdmin;
        
        if (req === undefined || 
            res === undefined || 
            isAdmin === undefined) {
            throw new Error('All params must be defined');
        }
        
        switch (req.method) {
            case 'GET':
                handleGet(req, res);
                break;
            case 'POST':
                handlePost(req, res);
                break;
            case 'PUT':
                if (!isAdmin) {
                    res.writeHead(401, 'Not admin', { 'WWW -Authenticate': 'Basic realm="quiz"' });
                    res.end();
                }
                handlePut(req, res);
                break;
            case 'DELETE':
                if (!isAdmin) {
                    res.writeHead(401, 'Not admin', { 'WWW -Authenticate': 'Basic realm="quiz"' });
                    res.end();
                }
                handleDelete(req, res);
                break;
            default:
                res.writeHead(400);
                res.end('HTTP method not known');
        }
    };
    
    // Retrieve question
    var handleGet = function (req, res) {
        console.log('GET');
        
        var url_parts = url.parse(req.url, true);
        var pathname = url_parts.pathname;
        console.log(pathname);
        switch (pathname) {
            case '/question':
                console.log('Question');
                
                var uid = getQueryString(req, 'uid');
                if (!uid) {
                    respond(res, { 'error' : 'input not valid' });
                }
                
                _api.getQuestion(uid, function (result) {
                    respond(res, result);
                });
                
                break;
            case '/user':
                console.log('User');
                
                var uid = getQueryString(req, 'uid');
                if (!uid) {
                    res.writeHead(400);
                    res.end('Parameter "uid" not present');
                }
                
                _api.getUser(uid, function (result) {
                    respond(res, result);
                });
                break;
            default:
                res.writeHead(400);
                res.end('URL not known');
        }
        
        
    };
    
    // Answer a question
    var handlePost = function (req, res) {
        console.log('POST');
        
        var uid = getQueryString(req, 'uid');
        var id = getQueryString(req, 'id');
        if (!uid || !id) {
            respond(res, { 'error' : 'input not valid' });
        }
        
        _api.answerQuestion(uid, id, function (result) {
            respond(res, { 'result': result });
        });
    };
    
    // (ADMIN) Create Question
    var handlePut = function (req, res) {
        console.log('PUT');
        
        var jsonString = '';
        req.on('data', function (data) {
            console.log('data: ' + data);
            jsonString += data;
        });
        req.on('end', function () {
            console.log('end');
            var json = JSON.parse(jsonString);
            console.dir(json);
            
            _api.createQuestion(json.text, json.alternatives, json.answer, function (item) {
                respond(res, item);
            });

            
        });
    };
    
    // (ADMIN) Delete question
    var handleDelete = function (req, res) {
        var uid = getQueryString(req, 'uid');
        if (!uid) {
            respond(res, { 'error' : 'input not valid' });
        }
        console.log('uid: ' + uid);
        _api.removeQuestion(uid, function (result) {
            respond(res, { 'result': result });
        });
    };
    
    var getQueryString = function (req, qs) {
        var url_parts = url.parse(req.url, true);
        var query = url_parts.query;
        return query[qs];
    }
    
    var respond = function (res, result) {
        result = JSON.stringify(result);
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(result);
    };
    
    var authenticate = function (req, res) {
        var authHeader = req.headers.authorization;
        
        if (!authHeader) {
            return util.getErrorPromise(401, 'Authorization header not present');
        }
        if (authHeader.toLowerCase().indexOf('basic ') === -1) {
            return util.getErrorPromise(401, 'Basic authorization not used');
        }
        
        var buf = new Buffer(authHeader.trim().substring(6), 'base64');
        var authParts = buf.toString().split(':');
        if (authParts.length !== 2) {
            return util.getErrorPromise(401, 'Credentials in wrong format [username:password]');
        }
        
        var username = authParts[0].trim();
        var password = authParts[1].trim();
        
        var deferred = Q.defer();
        
        _api.getUserByCredentials(username, password)
        .then(function (users) {
            console.log('checking users...');
            console.dir(users);
            
            if (users == null) {
                return deferred.reject(new Error('Internal Server Error'));
            }
            
            if (users.length > 1) {
                return deferred.reject(new Error('Duplicated users'));
            }
            if (users.length === 0) {
                return util.getErrorPromise(401, 'Wrong credentials');
            }
            
            var user = users[0];
            
            if (user.username === 'ultimate') {
                return deferred.resolve({ 'req': req, 'res': res, 'isAdmin': true });
            }
            else {
                return deferred.resolve({ 'req': req, 'res': res, 'isAdmin': false });
            }

        });
        //.fail(function (err) {
        //    console.log('error');
        //    return deferred.reject(err);
        //});

        return deferred.promise;
        
        //_api.getUserByCredentials(username, password, function (users) { 
        //    // TODO: Replace callback with promise
        //    if (users.length > 1) {
        //        throw new Error('Duplicated users');
        //    }
        //    if (users.length === 0) {
        //        return util.getErrorPromise(401, 'Wrong credentials');
        //    }
            
        //    var user = users[0];
            
        //    if (user.username === 'ultimate') {
        //        return Q.resolve({ 'req': req, 'res': res, 'isAdmin': true });
        //    }
        //    else {
        //        return Q.resolve({ 'req': req, 'res': res, 'isAdmin': false });
        //    }

        //});


    };
    
    var onFail = function (obj, res) { 
        var code = obj.code;
        var reason = obj.reason;
        var headers = obj.headers;
        
        if (code === undefined ||
            reason === undefined) {
            res.writeHead(500, 'Internal Server Error');
            res.end();
        }
        
        if (headers === undefined) {
            headers = {};
        }
        
        res.writeHead(code, reason, headers);
        res.end();
    };

    return {
        'handleRequest' : HandleRequest
    };
};