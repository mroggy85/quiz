﻿
// external modules
var Q = require('q');

// Internal modules
var util = require('./libs/util.js');
var baseController = require('./baseController.js');

module.exports = function (api) {
    if (api === undefined) {
        throw new Error('api must be passed');
    }
    var _api = api;
    var _baseController = baseController(api);
    
    // Retrieve question
    var handleGet = function (req, res) {        
        var uid = util.getQueryString(req, 'uid');
        if (uid === undefined) {
            return util.getErrorPromise(400, 'QueryString "uid" is not present');
        }
        
        return _api.getQuestion(uid, function (result) {
            return util.getOkPromise(result);
        });
    };
    
    // Answer a question
    var handlePost = function (req, res) {
        console.log('POST in questionController');
        
        return _baseController.extractJson(req)
        .then(function (json) {
            var uid = json.uid;
            var id = json.id; 
            if (uid === undefined || 
                id === undefined) {
                console.log('uid or id is undefined');
                return util.getErrorPromise(400, 'QueryString "uid" and "id" is not present');
            }

            return _api.answerQuestion(uid, id, function (result) {
                return util.getOkPromise(result);
            });
        });
    };
    
    // (ADMIN) Create Question
    var handlePut = function (req, res) {
        
        return _baseController.extractJson(req)
        .then(function (json) { 
            var text = json.text;
            var alternatives = json.alternatives;
            var answer = json.answer;
            
            if (text === undefined || 
                alternatives === undefined ||
                answer === undefined) {
                return util.getErrorPromise(400, 'The data "text", "alternatives" and "answer" is not present');
            }
            
            return _api.createQuestion(text, alternatives, answer, function (item) {
                return util.getOkPromise(item);
            });
        });

        //var jsonString = '';
        //req.on('data', function (data) {
        //    console.log('data: ' + data);
        //    jsonString += data;
        //});
        //req.on('end', function () {
        //    console.log('end');
        //    var json = JSON.parse(jsonString);
            
        //    var text = json.text;
        //    var alternatives = json.alternatives;
        //    var answer = json.answer;
            
        //    if (text === undefined || 
        //        alternatives === undefined ||
        //        answer === undefined) {
        //        return util.getErrorPromise(400, 'The data "text", "alternatives" and "answer" is not present');
        //    }
            
        //    _api.createQuestion(text, alternatives, answer, function (item) {
        //        return util.getOkPromise(item);
        //    });

            
        //});
    };
    
    // (ADMIN) Delete question
    var handleDelete = function (req, res) {
        var uid = util.getQueryString(req, 'uid');
        if (uid === undefined) {
            return util.getErrorPromise(400, 'QueryString "uid" is not present');
        }

        _api.removeQuestion(uid, function (result) {
            return util.getOkPromise(result);
        });
    };
    
    _baseController.events.onGet = handleGet;
    _baseController.events.onPost = handlePost;
    _baseController.events.onPut = handlePut;
    _baseController.events.onDelete = handleDelete;

    return {
        'handleRequest' : _baseController.handleRequest
    };
};