
var question = {
    "_id": 0,
    "question": "",
    "alternatives": [],
    "answer": 0
};

var uuid = require('node-uuid');

exports.getEmptyQuestion = function () {
    return question;
};

exports.getQuestion = function (text, alternatives, answer) {
    if (!text || !alternatives || !answer) { 
        return null;
    }
    
    var q = question
    q._id = uuid.v4();
    q.question = text;
    q.alternatives = alternatives;
    q.answer = answer;

    return q;
};